# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='ballotbot',
    version='0.0.1',
    description='Secret ballot bot for Discord',
    long_description = readme,
    author='Alex Redpath',
    author_email='adredpath97@yahoo.co.uk',
    url='https://bitbucket.org/kazantu/ballotbot/',
    license=license,
    packages=find_packages()
)