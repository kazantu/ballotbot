import discord
from discord.ext import commands
from cog import BallotBotCog

with open('TOKEN') as f:
    token = f.read()

bot = commands.Bot(command_prefix='ballotbot!')
bot.add_cog(BallotBotCog(bot))

@client.event
async def on_ready():
    print('BallotBot logged in as {0.user}'.format(bot))

bot.run(token)